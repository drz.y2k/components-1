import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'

//Instalamos bootstrap e importamos los css de este modo
import 'bootstrap/dist/css/bootstrap.min.css';

createApp(App).mount('#app')
